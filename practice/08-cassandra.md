## Apache Cassandra
Cassandra — распределённая NoSQL база данных. 
Cassandra считается гибридом key-value и column-oriented баз данных. 

## Особенности Apache Cassandra.
* База данных типа key-value
* CQL — это SQL-подобный язык. Аббревиатура от Cassandra Query Language.
* Основная единица хранения — строка. Строка целиком хранится на нодах, т.е. нет ситуаций когда полстроки — 
на одной ноде, полстроки — на другой. Строка может динамически раширяться до 2 миллиардов колонок.
* Строка может храниться на нескольких узлах - репликация.  Если у вас фактор репликации N и вы погасили N-1 машин, данные будут и дальше доступны.
* Помимо обычных значений, вроде целых чисел, строк и дат, в столбцах могут храниться коллекции, например, map<T1, T2>, list<T> и set<T>
* При каждом чтении и записи клиент может указать желаемый уровень консистентности — ANY, ONE, TWO, THREE, QUORUM, SERIAL, ALL и другие. 
* Например, уровень ONE (используется по умолчанию) говорит, что запрос должен дойти хотя бы до одного узла, отвечающего за хранение строки, 
 а уровень QUORUM — что запрос должно получить большинство узлов, например, 2 из 3. Таким образом, можно всегда выбирать между скоростью выполнения запросов и надежностью. 
 По умолчанию в фоне ноды Cassandra работает read repair процесс, приводящий все ноды в консистентное состояние, поэтому для многих задач ONE является 
 вполне подходящим уровнем
* Помимо clustering key есть еще и вторичные индексы.

## Важные файлы и каталоги:
* /etc/cassandra/cassandra.yaml — основные настройки;
* /etc/cassandra/cassandra-env.sh — все параметры JVM;
* /var/log/cassandra/system.log — смотрим сюда, если что-то сломалось;
* /var/lib/cassandra/ — все данные;


## Установка Cassandra.
* Для работы Cassandra необходимо Java версии 7 - 8. Перед установкой Cassandra необходимо поставить JDK - https://www.oracle.com/technetwork/java/javase/downloads/index.html.
С версиями 9+ Cassandra не работает. Проверить последнюю установленную версию - /usr/libexec/java_home -v
* Если вы уже запускали Cassandra, то перед следующим запускам необходимо завершить все предыдущие CassandraDaemon,
иначе появится ошибка, что порт уже занят. Сделать это можно командой pkill -f 'java.*cassandra'

### Установка Cassandra локально для macOs
* Загрузка ПО DataStax. Содержит последнюю версию Cassandra, утилиту Cassandra Query Language (CQL),
инструмент визуализации OpsCenter. Выбираем где хранить Cassandra, устанавливаем 
curl -OL http://downloads.datastax.com/community/dsc.tar.gz
и распаковываем
tar -xzf dsc-cassandra-1.2.2-bin.tar.gz
* Переходим в соответствующую директорию Cassandra cd dsc-cassandra-3.0.9/bin
* Установка sudo ./cassandra
* В той же директории устанавливаем утилиту CQL: ./cqlsh

### Установка Cassandra локально для Ubuntu

* Необходимо установить Python 2.7.11 или младше
* curl -L http://debian.datastax.com/debian/repo_key | sudo apt-key add -
* echo "deb http://debian.datastax.com/community stable main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list
* sudo apt-get update
* sudo apt-get install dsc21 cassandra-tools
* Альтернатива: через Datastax - https://academy.datastax.com/planet-cassandra/cassandra

Запуск/остановка Cassandra:
* sudo service cassandra start
* sudo service cassandra stop

### Установка Cassandra локально для Windows
* Скачать установочный файл Datastax community edition для Windows - https://academy.datastax.com/planet-cassandra/cassandra
* Запустить консоль Cassandra CQL Shell

[Полное руководство](https://www.digitalocean.com/community/tutorials/how-to-run-a-multi-node-cluster-database-with-cassandra-on-ubuntu-14-04) по установке Cassandra в распределённом режиме для prod.

## Подключение к кластеру 
ssh USER@mipt-client.atp-fivt.org

## Начало работы с Cassandra
Войти в cql командой: `cqlsh <node>`. Выйти из него - exit.

 KeySpace - пространство имен, которое определяет репликацию данных на узлы. Изначально кластер содержит отдельное 
 пространство имен для каждого узла
 Можно создать свое пространство ключей:
 
```code
CREATE KEYSPACE “KeySpace Name”
WITH replication = {'class': ‘Strategy name’, 'replication_factor' : ‘No.Of  replicas’}

AND durable_writes = ‘Boolean value’;

```
Команда создания пространства имен содержит два параметра: replication_factor, durable_writes, которые преставляют
стратегию репликации и свойство, что при записе происходит ожидание, пока запись произойдет на все реплики.
Стратегии репликации:
* Simple Strategy. Рекомендуется использовать при одном дата центре. Первая реплика помещается на узел в соответствии с распределением,
остальные - на узлы в порядке часовой стрелки. 
* Network Topology Strategy. В кластере более одного дата центра. Необходимо представить фактор репликации для каждого дата центра отдельно.

Replication Factor - число реплик записи на различных узлах. 

Пример:

```cqlsh
Create keyspace University with replicaton={'class':'SimpleStrategy','replication_factor': 3};
```


#### Пространство ключей

Посмотреть все пространства ключей Cassandra:

```code
DESCRIBE keyspaces;
```

Посмотреть свойства пространства ключей:
```code
SELECT * FROM system_schema.keyspaces;
```

Удаление пространства ключей:

```code
DROP KEYSPACE [IF EXISTS] keyspace_name;
```

При изменении конфигурации пространства ключей необходимо учитывать следующее:
* Имя пространства не может быть изменено
* Параметр DURABLE_WRITES по умолчанию true
* Стратегия рапликации и параметр DURABLE_WRITES могут быть изменены

Изменение конфигурации пространства ключей:
```code
Alter Keyspace KeyspaceName with replication={'class':'StrategyName', 
	'replication_factor': no of replications on different nodes} 
    	with DURABLE_WRITES=true/false
```

CQL выражения можно выполнять в любом текстовом редакторе и запускать на кластере командой SOURCE:
```code
SOURCE '~/cycling_setup/create_ks_and_tables.cql'
```

#### Информацию можно выгружать из csv файлов напрямую в Cassandra. 
```code
COPY cycling.cyclist_name (first,second) 
FROM '../info.csv' WITH HEADER = TRUE ;
```
Также данные из таблиц можно загружать в csv:
```code
COPY cycling.cyclist_name (first,second) 
TO '../info.csv' WITH HEADER = TRUE ;
```
HEADER - включение имен колонок первой линии.

Переключиться на другое пространство ключей: 
```code 
USE otherKeySpace;
```

#### Синтаксис создания таблицы.
```code
CREATE TABLE tablename(
   column1_name data_type PRIMARY KEY,
   column2_name data_type,
   column3_name data_type
   )
```
В примере выше представлен самый простой вариант первичного ключа. В пределах колоночного семейства запись уникально 
идентифицируется значением идентификатора объекта. Весь первичный ключ в этом примере будет являться ключом раздела (partition key), 
по которому Cassandra определит на каком узле/узлах будет храниться данная запись.

   С помощью композитного ключа
```code
CREATE TABLE tablename(
   column1_name data_type,
   column2_name data_type,
   column3_name data_type,
   PRIMARY KEY (column1, column2)
   )
```
В ситуации COMPOSITE (составной первичный ключ) первичный ключ "первая часть" ключа называется PARTITION KEY 
(в этом примере column1 ключ раздела), а вторая часть ключа - КЛАСТЕРНЫЙ КЛЮЧ (column2).

![IMAGE ALT TEXT HERE](assests/Primary_key.png)

Поскольку теперь первичный ключ будет идентифицироваться двумя полями, то если хотя бы одно из них отличается, 
то фактически это уже должна быть другая запись. Но на самом деле ситуация будет немного сложнее: если ключ раздела будет у двух 
записей один и тот же, а кластерный ключ будет отличаться, то это в итоге будет одна  большая запись, составленная из двух. 
Главное отличие от простого первичного ключа состоит в том, что записи с отличающимися значениями 
ключа группировки добавляются к существующей записи с идентичным ключом. Таким образом можно, например, сохранять историю изменений записей
с одним и тем же первичным ключем.

![IMAGE ALT TEXT HERE](assests/Cluster_key.png)

Обратите внимание, что ключ раздела и ключ кластеризации могут быть представлены несколькими столбцами:
```code
CREATE TABLE tablename(
   column1_name data_type,
   column2_name data_type,
   column3_name data_type,
   column4_name data_type,
   column5_name data_type,
   column6_name data_type,
   PRIMARY KEY ((column1, column2), column3, column4)
   )
```

PRIMARYKEY используется для уникальной идентификации строки. 

#### Получение информации о всех таблицах в кластере:

```code
SELECT * FROM system_schema.tables WHERE keyspace_name = 'keyspace name';
```

О какой-то одной таблице:

```code
SELECT * FROM system_schema.columns 
WHERE keyspace_name = 'keyspace_name' AND table_name = 'table_name';
```

## Задача. Сервис MyTube.
Данные для таблиц лежат в директории /data/cassandra_practice_data.

Правильная команда запуска
```code
cqlsh --cqlversion="3.4.0"
```
### Проверка работоспособности сессии.
* Запустите CQL shell командой cqlsh
* Выведите список всех схем, выолнив команду
```code
SELECT * FROM system_schema.keyspaces;
```

### Получение информации о сущностях.
* В cqlsh выполните команду HELP
* Найдите способ получить информацию о схеме system. Какая стратегия репликации используется в таблицах этой схемы?

### Создание схемы
* В cqlsh создайте схему для таблиц видеосервиса MyTube, назовите схему логином, 
который вы используете для входа на кластер. В качестве стратегии репликации используйте 'SimpleStrategy', фактор репликации 2.
* Выведите информацию о созданной схеме, используя знания, полученные в предыдущем пункте

### Создание таблицы


| Название | Тип данных |
| ------ | ------ |
| video_id | timeuuid |
| added_date | timestamp | 
| description | text | 
| title | text | 
| user_id | uuid | 


* Откройте файл в любом удобном вам текстовом редакторе (nano, vim) и напишите код для создания таблицы video со схемой, приведенной выше
* Запустите cqlsh и командой USE переключитесь в схему, которую вы создали в предыдущем пункте
* Используйте команду SOURCE для того, чтобы создать таблицу кодом из файла
* Используйте команду COPY для того, чтобы загрузить в таблицу данные из файла ex4/videos.csv
* Убедитесь, что данные загружены, сделав выборку

### Композитные ключи партиционирования


| Название | Тип данных |
| ------ | ------ |
| title | text |
| added_year | int | 
| added_date | timestamp | 
| description | text | 
| user_id | uuid | 
| video_id | uuid | 


* Создайте таблицу videos_by_title_year со схемой, приведенной выше. 
В качестве ключа партиционирования задайте композитный ключ из двух столбцов title и added_year
* Используйте команду COPY, чтобы загрузить данные из файла ex5/videos_by_title_year.csv
* Сделайте выборку из таблицы, одновременно применив фильтр и по title и по added_year
* Попробуйте сделать выборку с фильтром только по полю added_year. Какую ошибку возвращает C*? Как вы думаете, почему?

### Кластерные ключи


| Название | Тип данных |
| ------ | ------ |
| tag | text |
| added_year | int | 
| video_id | uuid | 
| added_date | timestamp | 
| description | text | 
| title | text | 
| user_id | uuid | 

* Необходимо создать таблицу video_by_tag_year для выполнения выборок видео по 
определенному тегу в заданном временном промежутке. Более свежие видео должны выдаваться раньше
* Заполните таблицу данными из файла ex6/videos_by_tag_year.csv
* Обратите внимание на то, что составной первичный ключ должен не только содержать кластерный ключ, но и обеспечивать уникальность!
* Сделайте выборку по конкретному тегу и какому-нибудь диапазону лет.
* Попробуйте сделать выборку без указания тега


## Python API для Cassandra.
Перед выполнением запросов в Cassandra необходимо установить экземпляр кластера. 
```python
from cassandra.cluster import Cluster

cluster = Cluster() # локальный запуск
```
Можно задать список IP адресов в кластере:
```python 
from cassandra.cluster import Cluster

cluster = Cluster(['192.168.0.1', '192.168.0.2'])
```

Список IP адресов - это точки подключения к кластеру. После того, как драйвер подключится к одному 
из улов, указанных в списке, ему автоматически раскроются другие адреса, и он подключится к ним. 
Необязательно указывать каждый узел в кластере.

Подключение к узлу с заданным пространством имен и выполнение запроса в кластер происходит
следующим образом:

```python
from cassandra.cluster import Cluster

cluster = Cluster(['192.168.0.1', '192.168.0.2'])
session = cluster.connect('mykeyspace')

rows = session.execute('SELECT name, age, email FROM users')
for user_row in rows:
    print user_row.name, user_row.age, user_row.email
```

После создания сессии пространство имен все еще можно сменить:

```python
session.set_keyspace('users')
# или таким образом
session.execute('USE users')
```

### Выполнение запросов
По умолчанию каждая строка в выборке запроса представлена как namedtuple - https://docs.python.org/2/library/collections.html#collections.namedtuple

Можно также произвести выборку строк по позиции:

```python
rows = session.execute('SELECT name, age, email FROM users')
for row in rows:
    print row[0], row[1], row[2]
```


### Асинхронные запросы
Асинхронные запросы позволяют не ждать выполнение запроса, вместо этого происходит возвращение ResponseFuture.

```python
from cassandra import ReadTimeout

query = "SELECT * FROM users WHERE user_id=%s"
future = session.execute_async(query, [user_id])

# ... do some other work

try:
    rows = future.result()
    user = rows[0]
    print user.name, user.age
except ReadTimeout:
    log.exception("Query timed out:")
```



## Java API для Cassandra
https://www.baeldung.com/cassandra-with-java

